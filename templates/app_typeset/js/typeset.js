"use strict"

var price_data = [];
var offer_data = [];

const DEFAULT_SELECTED_PLAN = "Bronze";
const OFFER_URL = "https://my-json-server.typicode.com/TypesetIO/mock/offers";
const PLAN_URL = "https://my-json-server.typicode.com/TypesetIO/mock/plans";

///////////////////////////////////////////
// Defining async function
async function get_plan_meta_api(url) {
	// Storing response
	const response = await fetch(url);
	// Storing data in form of JSON
	var data = await response.json();
	console.log(data);
	if (response) {
		hideloader();
	}
	show_plan_meta_api(data);
}

// Calling that async function
get_plan_meta_api(PLAN_URL);

// Function to define innerHTML for HTML table
function show_plan_meta_api(data){
    price_data = data;
    render_plan_card(price_data);
    home_event_handler();

    get_offer_meta_api(OFFER_URL);
    showloader();
}


// Defining async function
async function get_offer_meta_api(url) {
	// Storing response
	const response = await fetch(url);
	// Storing data in form of JSON
	var data = await response.json();
	console.log(data);
	if (response) {
		hideloader();
	}
	show_offer_meta_api(data);
	hideloader();
}

// Function to define innerHTML for HTML table
function show_offer_meta_api(data){
    offer_data = data;
    render_active_plan();
    home_event_handler();
    $(".loader_div").hide();
}

// Function to hide the loader
function hideloader() {
	 $(".loader_div").hide();
}

function showloader() {
	 $(".loader_div").show();
}


jQuery(document).ready(function () {

});

function render_plan_card(data){
    var plan_data =``;

    for(var i in data){
        var id = data[i]["id"];
        var title = data[i]["title"];
        var price = data[i]["price"];
        var selected_class = '';

        plan_data += `<div class="plan_card ${selected_class}" data-id="${id}">
                        <div class="plan_title">${title}</div>
                        <div class="plan_price">${price}</div>
                    </div>`;
    }

    $("#plan_container").html(plan_data);
}

function render_active_plan(){
    var active_plan_id = sessionStorage['plan'];

    if(active_plan_id){
        $(`.plan_card[data-id="${active_plan_id}"]`).addClass('active');
        for(var i in offer_data){
            var planId = offer_data[i]["planId"];
            if(active_plan_id == planId){
                render_offer(offer_data[i]);
            }
        }
    }
    else{
       for(var i in price_data){
            var id = price_data[i]["id"];
            var title = price_data[i]["title"];
            if(title == DEFAULT_SELECTED_PLAN){
                sessionStorage['plan'] = id;
            }
        }
        render_active_plan();
    }
}

function render_offer(data){
    var offer_html = ``;
    var id = data["id"]
    var body = data["body"]
    var planId = data["planId"]
    var expires = data["expires"]
    var days = expires["days"]
    var hours = expires["hours"]
    var minutes = expires["minutes"]
    var seconds = expires["seconds"]

    offer_html = `<div class="active_offer_wrapper">
                    <div class="active_offer" data-id="${id}" data-plan="${planId}">
                        <h1 class="offer_body" >${body}</h1>
                        <div class="expire_container">
                            <div>Offer expires in </div>
                            <p class="offer_expires" id="timer_${id}">${days}:${hours}:${minutes}:${seconds}</p>
                        </div>
                    </div>
                    <div class="active_offer_space"></div>
                </div>`;

    $(".offer_content").append(offer_html);

    show_countdown_fn(data);
}




function home_event_handler(){
    $(".plan_card").unbind("click.plan_card", plan_card_fn);
    $(".plan_card").bind("click.plan_card", plan_card_fn);
}


function plan_card_fn(){
    var id = $(this).data('id');
    sessionStorage['plan'] = id;
    location.reload();
}



function show_countdown_fn(data){
    var id = data["id"]
    var expires = data["expires"]
    var days = expires["days"]
    var hours = expires["hours"]
    var minutes = expires["minutes"]
    var seconds = expires["seconds"]

    var elem_id = `timer_${id}`;

    var display = document.querySelector(`#${elem_id}`);

    var days_sec = days * 24 * 60 * 60;
    var hours_sec = hours * 60 * 60;
    var minutes_sec = minutes * 60;

    var time_in_sec = days_sec + hours_sec + minutes_sec + seconds;

    startTimer(time_in_sec, display);
}

function startTimer(duration, display) {
    var seconds = duration;
    setInterval(function () {
        var days        = Math.floor(seconds/24/60/60);
        var hoursLeft   = Math.floor((seconds) - (days*86400));
        var hours       = Math.floor(hoursLeft/3600);
        var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
        var minutes     = Math.floor(minutesLeft/60);
        var remainingSeconds = seconds % 60;
        function pad(n) {
            return (n < 10 ? "0" + n : n);
        }
          display.textContent = pad(days) + ":" + pad(hours) + ":" + pad(minutes) + ":" + pad(remainingSeconds);

        if (--seconds < 0) {
            seconds = duration;
        }
    }, 1000);
}

function reset_offer(){
    $(".offer_content").append("");
}







