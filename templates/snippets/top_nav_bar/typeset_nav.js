var wlp = window.location.pathname;
$(document).ready(function() {


    if (
    (wlp == '/')
    ){
        $('.top_nav_a').removeClass('active');
        $('.top_nav_dashboard_div .top_nav_a').addClass('active');
    }

    if (
        (wlp == '/upload/')||
        (wlp.indexOf('/upload/') > -1)
    ){
        $('.top_nav_a').removeClass('active');
        $('.top_nav_upload_div .top_nav_a').addClass('active');
    }

    if (
        (wlp == '/search/')||
        (wlp.indexOf('/search/') > -1)
    ){
        $('.top_nav_a').removeClass('active');
        $('.top_nav_search_div .top_nav_a').addClass('active');
    }

    if (
        (wlp == '/result/')||
        (wlp.indexOf('/result/') > -1)
    ){
        $('.top_nav_a').removeClass('active');
        $('.top_nav_result_div .top_nav_a').addClass('active');
    }

    if (
        (wlp == '/jobs/')||
        (wlp.indexOf('/jobs/') > -1)
    ){
        $('.top_nav_a').removeClass('active');
        $('.top_nav_jobs_div .top_nav_a').addClass('active');
    }

    if (
        (wlp == '/watchlist/')||
        (wlp.indexOf('/watchlist/') > -1)
    ){
        $('.top_nav_a').removeClass('active');
        $('.top_nav_watchlist_div .top_nav_a').addClass('active');
    }


});

$("a").click(function(){
    if(this.id.indexOf("logout") > -1){
        sessionStorage.clear();
    }
});
