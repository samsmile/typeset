from django.apps import AppConfig


class AppTypesetConfig(AppConfig):
    name = 'app_typeset'
