from django.shortcuts import render

# Create your views here.
def typeset_view(request):
    context_payload = {
        "data": ""
    }
    return render(request, 'app_typeset/html/index.html', context_payload)
