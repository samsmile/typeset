from django.urls import path, include
from app_typeset import views

app_name = 'app_typeset'

urlpatterns = [
    path('', views.typeset_view, name='typeset_view'),
]
